# Release Notes - {Product Name} {Release Number - 0.0.0}

{Before using this template, read the accompanying [release notes template guide](release-notes-template-guide.md)}.

## {Optional: Release Notes Version - 0}
## {Release Date - YYYY-MM-DD}

{Optional: High-level summary}

### New features

- **{Feature title}**

  {Feature description}

### Improvements

- **{Improvement title}**

  {Improvement description}

### Bug fixes

- **{Bug fix title}**

  {Bug fix description}

### Known issues

- **{Known issue title}**

  {Known issue description}

### Optional: Deprecated features

- **{Deprecated feature title}**

  {Deprecated feature description}
